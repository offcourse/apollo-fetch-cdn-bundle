export default [{
  input: 'node_modules/apollo-fetch/dist/apollo-fetch.js',
  external: ['cross-fetch/polyfill'],
  output: {
    file: 'dist/apollo-fetch-cdn-bundle.js',
    name: 'apolloFetch',
    format: 'iife'
  }
}];
